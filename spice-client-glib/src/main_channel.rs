use glib::ffi::gpointer;
use glib::gobject_ffi::GObject;
use glib::object::ObjectType as ObjectType_;
use glib::signal::connect_raw;
use glib::signal::SignalHandlerId;
use glib::translate::*;
use std::boxed::Box as Box_;
use std::mem::transmute;

use crate::MainChannel;

impl MainChannel {
    pub fn connect_main_clipboard_selection<F: Fn(&MainChannel, u32, u32, &[u8]) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn main_clipboard_selection_trampoline<
            F: Fn(&MainChannel, u32, u32, &[u8]) + 'static,
        >(
            this: *mut ffi::SpiceMainChannel,
            selection: libc::c_uint,
            type_: libc::c_uint,
            data: glib::ffi::gpointer,
            size: libc::c_uint,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(
                &from_glib_borrow(this),
                selection,
                type_,
                &FromGlibContainerAsVec::from_glib_none_num_as_vec(data as *const u8, size as _),
            )
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"main-clipboard-selection\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    main_clipboard_selection_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    pub fn connect_main_clipboard_selection_grab<F: Fn(&MainChannel, u32, &[u32]) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn main_clipboard_selection_trampoline<
            F: Fn(&MainChannel, u32, &[u32]) + 'static,
        >(
            this: *mut ffi::SpiceMainChannel,
            selection: libc::c_uint,
            types: glib::ffi::gpointer,
            ntypes: libc::c_uint,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(
                &from_glib_borrow(this),
                selection,
                &FromGlibContainerAsVec::from_glib_none_num_as_vec(
                    types as *const u32,
                    ntypes as _,
                ),
            )
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"main-clipboard-selection-grab\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    main_clipboard_selection_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    pub fn file_copy_async<F:Fn(i64, i64) + 'static, P:Fn(&Self, bool, Option<glib::Error>) + 'static>(&self, sources: &[&str], flags: gio::FileCopyFlags, progress_callback:F, callback: P) {
        unsafe { 
         unsafe extern "C" fn _progress_callback<F: Fn(i64, i64) + 'static>(
             _current_num_bytes: i64,
             _total_num_bytes: i64,
             _data: glib::ffi::gpointer,
         ) {
             let f: &F = &*(_data as *const F);
             f(_current_num_bytes, _total_num_bytes);
         }
 
         unsafe extern "C" fn _callback<F: Fn(&MainChannel, bool, Option<glib::Error>) + 'static>(
             this: *mut ffi::SpiceMainChannel,
             result: *mut gio::ffi::GAsyncResult,
             data: glib::ffi::gpointer,
         ) {
             let f: &F = &*(data as *const F);
             let mut error_ptr = std::ptr::null_mut();
             let success = ffi::spice_main_channel_file_copy_finish(this, result, &mut error_ptr) == 1;
             let mut error = None;
             let this:&MainChannel = &from_glib_borrow(this);
 
             if !error_ptr.is_null() {
                 error = Some(from_glib_full(error_ptr));
             }
 
             f(this, success, error);
         }
 
         let progress_callback_ptr: glib::ffi::gpointer = Box_::into_raw(Box_::new(progress_callback)) as *mut _;
         let callback_ptr: glib::ffi::gpointer = Box_::into_raw(Box_::new(callback)) as *mut _;
         let callback = Some(std::mem::transmute::<_, unsafe extern "C" fn(*mut GObject, *mut gio::ffi::GAsyncResult, gpointer)>(
             _callback::<P> as *const (*mut GObject, *mut gio::ffi::GAsyncResult, gpointer),
         ));
 
         let files = sources.iter().map(|elem|{
             gio::File::for_uri(&elem)
         }).collect::<Vec<gio::File>>();
 
         ffi::spice_main_channel_file_copy_async(
             self.to_glib_none().0, 
             files.to_glib_none().0, 
             flags.into_glib(), 
             gio::Cancellable::NONE.to_glib_none().0, 
             Some(_progress_callback::<F>), 
             progress_callback_ptr, 
             callback,
             callback_ptr
             ) }
     }
}
